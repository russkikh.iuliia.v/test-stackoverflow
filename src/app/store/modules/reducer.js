import {SET_DATA, PLUS_SCORE} from './actions';

const initialState = {
  postsList: []
};

export const postsReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_DATA:
      return {
        ...state,
        postsList: action.posts,
      };
    case PLUS_SCORE:
      return {
        ...state,
        postsList: {
          items: state.postsList.items.map((item) => {
            if (item.question_id === action.postId) {
              if (action.changeScore === 'up') item.score += 1;
              else if (action.changeScore === 'down') item.score -=1;
            }
            return item;
          }),
        }
      };
    default:
      return state;
  }
};
