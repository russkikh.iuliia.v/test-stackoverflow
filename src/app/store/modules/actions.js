export const SET_DATA = 'SET_DATA';

export const setPosts = (posts) => {
  return {
    type: SET_DATA,
    posts,
  }
};

export const PLUS_SCORE = 'PLUS_SCORE';

export const plusScore = (postId, changeScore) => {
  return {
    type: PLUS_SCORE,
    postId,
    changeScore,
  }
}
