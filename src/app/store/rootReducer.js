import { combineReducers } from 'redux';
import { postsReducer } from "./modules/reducer";

export const rootReducer = combineReducers({
  posts: postsReducer,
});
