import { createStore } from 'redux';
import { rootReducer } from "./rootReducer";

export const store = createStore(
  rootReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

const subscribe = () => {
  console.log(JSON.stringify(store.getState(), null, 2))
};

store.subscribe(subscribe);
