import React from 'react';
import DatePicker from "react-datepicker";

export const DatePickerContainer = (props) => {
  const {startDate, checkDate} = props;
  return (
    <DatePicker
      dateFormat="dd.MM.yyyy"
      selected={startDate}
      onChange={checkDate}
    />
  )
}
