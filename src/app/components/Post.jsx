import React, {useState} from 'react';
import {ReactComponent as Arrow} from "../icons/arrow.svg";

export const Post = (props) => {
  const {postInfo, className, plusScore} = props;
  const classes = `${className} post ${postInfo.is_answered ? 'post_answered' : ''}`;
  const clickHandler = (e) => {
    plusScore(postInfo.question_id, e.target.getAttribute('score'));
  };
  const getDate = () => {
    const newDate = new Date(postInfo.creation_date * 1000);
    return `${newDate.getDate()}.${newDate.getMonth()}.${newDate.getFullYear()}`
  };
  const [maxHeight, setMaxHeight] = useState(0);
  const panelOpen = () => {
    setMaxHeight(prevState => 1);
  };

  return (
    <li className={classes}>
      <span
        className="post__trigger"
        onClick={panelOpen}
      >
        <h4 className="post__title">{postInfo.title}</h4>
        <span className='post__score'>
          <Arrow score='up' onClick={clickHandler} className='post__score-arrow post__score-arrow_up'/>
          <span className="post__score-count">{postInfo.score}</span>
          <Arrow score='down' onClick={clickHandler} className='post__score-arrow post__score-arrow_down'/>
        </span>
      </span>
      <span
        className="post__panel"
        style={{
          maxHeight: maxHeight + 'px',
        }}
      >
        <span className='post__panel-cover'>
          <p className="post__info">Имя создателя вопроса: <span>{postInfo.owner.display_name}</span></p>
          <p className="post__info">Количество просмотров вопроса: <span>{postInfo.view_count}</span></p>
          <p className="post__info">Дата создания: <span>{getDate()}</span></p>
        </span>
      </span>
    </li>
  )
};
