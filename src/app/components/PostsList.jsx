import React from 'react';
import { Post } from "./Post";

export const PostsList = (props) => {
  const {posts, plusScore} = props;
  return (
    <ul className="posts">
      {posts.map((item, index) => (
        <Post plusScore={plusScore} className='posts__item' key={index} postInfo={item} />
      ))}
    </ul>
  )
};
