import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from "react-redux";
import {plusScore, setPosts} from "./app/store/modules/actions";
import { PostsList } from "./app/components/PostsList";
import './main.scss';
import {DatePickerContainer} from "./app/components/DatePickerContainer";

export const App = () => {
  const dispatch = useDispatch();
  const [opacity, setOpacity] = useState({opacity: 0, visibility: 'hidden'});
  const [startDate, setStartDate] = useState(new Date(2018, 0, 1));
  let posts = useSelector(state => state.posts.postsList.items);
  if (!posts) posts = [];

  useEffect(() => {
    // key=Rw9YPXRYpf3Rn66*v9DVsw((
    fetch('https://api.stackexchange.com/2.2/search?&pagesize=5&fromdate=1514764800&order=desc&sort=votes&intitle=react-redux&site=stackoverflow&filter=!LalfoEU0HS0KH2ZNMQ19_i')
      .then(response => response.json())
      .then(data => dispatch(setPosts(data)))
      .catch(error => console.log(error));

  }, []);

  const clickHandler = (postId, changeScore) => {
    dispatch(plusScore(postId, changeScore));
  };

  const checkDate = (date) => {
    setStartDate(date);
    if (date !== startDate) setOpacity({opacity: 1, visibility: 'visible'});
  };

  const handleClick = () => {
    fetch(`https://api.stackexchange.com/2.2/search?&pagesize=5&fromdate=${startDate.getTime() / 1000}&order=desc&sort=votes&intitle=react-redux&site=stackoverflow&filter=!LalfoEU0HS0KH2ZNMQ19_i`)
      .then(response => response.json())
      .then(data => dispatch(setPosts(data)))
      .catch(error => console.log(error));
    setOpacity({opacity: 0, visibility: 'hidden'});
  };

  useEffect(() => {

  }, [startDate]);

  return (
    <div className="App home">
      <div className="container">
        <h1 className='home__title'>5 самых популярных вопросов на Stackoverflow, содержащих строку "react-redux" в
          наименовании.</h1>
        <div className="home__date">
          <span>Начальная дата:</span>
          <div className="home__datepicker">
            <DatePickerContainer
              dateFormat="dd.MM.yyyy"
              startDate={startDate}
              checkDate={checkDate}
            />
          </div>
          <button
            className='home__search'
            style={opacity}
            onClick={handleClick}
          >
            Поиск</button>
        </div>
        <PostsList plusScore={clickHandler} posts={posts} />
      </div>
    </div>
  );
};
